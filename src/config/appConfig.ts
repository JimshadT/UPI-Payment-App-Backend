/**
 * Created by rohittalwar on 15/01/16.
 */

import { Provider } from "nconf";
let nconf = new Provider({});

var currentEnv = process.env.NODE_ENV || "development";
nconf
    .argv()
    .env()
    .file({ file: require.resolve("./" + currentEnv + ".json") });

const env_map = {
    db_base_url: "db:base_url",
    db_name: "db:name",
};

for (let key in env_map) {
    env_map[key] && nconf.set(env_map[key], nconf.get(key));
}
export default nconf;
