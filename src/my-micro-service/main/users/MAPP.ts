import MAPPGQLRoot from "./MAPPGQLRoot";
import MAPPCors from "./MAPPCors";
import * as express from "express";
import { service_validation } from "./rest-apis/service-validation";

var MAPP = express.Router();

MAPP.options("*", MAPPCors);
MAPP.use(MAPPCors);
MAPP.get("/service", service_validation);
MAPP.use(MAPPGQLRoot);

export default MAPP;
