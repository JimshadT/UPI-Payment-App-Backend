import * as express from "express";
const jwt = require("jsonwebtoken");

export const service_validation = (req: express.Request, res: express.Response, next: express.NextFunction) => {
    if (req.headers && req.headers["authorization"]) {
        const token = req.headers["authorization"].slice(7);

        jwt.verify(token, "secret101", function (err, decoded) {
            if (decoded) {
                return res.status(200).json(decoded);
            } else {
                return res.status(401).json({ status: "401 Unauthorized" });
            }
        });
    } else {
        return res.status(401).json({ status: "401 Unauthorized" });
    }
};
