"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.service_validation = void 0;
const jwt = require("jsonwebtoken");
const service_validation = (req, res, next) => {
    if (req.headers && req.headers["authorization"]) {
        const token = req.headers["authorization"].slice(7);
        jwt.verify(token, "secret101", function (err, decoded) {
            if (decoded) {
                return res.status(200).json({ decoded });
            }
            else {
                return res.status(401).json({ status: "401 Unauthorized" });
            }
        });
    }
    else {
        return res.status(401).json({ status: "401 Unauthorized" });
    }
};
exports.service_validation = service_validation;
