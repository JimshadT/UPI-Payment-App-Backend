import { GQLNow } from "gql-now";
import myAssert from "../../../helpers/Error_Handler";
import token_creation from "../../../helpers/jwt_authentication";
import DBInterface from "../../../db-schemas";
var trn_id = "TR";
const db = new DBInterface();
import { GraphQLError } from "graphql";

const send_money_to_upi = async (args: any, req: any, user: any) => {
    try {
        let fromUser: any = await db.users.findOne({ _id: user.id }).lean().exec();
        myAssert(fromUser === null, "user not found");

        let toUser = args.upi_id;

        myAssert(args.amount > fromUser.available_creditline, "Insufficient money");

        let payout_payload = {
            account_number: "3434866817852974",
            amount: args.amount * 100,
            currency: "INR",
            mode: "UPI",
            purpose: "payout",
            fund_account: { account_type: "vpa", vpa: { address: args.upi_id }, contact: { name: args.name ?? "user" } },
        };

        let obj = new GQLNow("http://api-ma-upi-payments-app.kred.cloud/v1/razorpay-payouts", {
            "x-access-token": token_creation({ id: fromUser?._id }),
        });

        let response = await obj.mutation("payout_to_vpa", payout_payload, "id entity fund_account_id status purpose");

        myAssert(typeof response.errors !== "undefined", response.errors);

        let lessCredit: number = parseFloat((fromUser.available_creditline - args.amount).toFixed(2));
        //let addCredit:number = parseFloat((toUser.available_creditline + args.amount).toFixed(2));

        await db.users.updateOne({ _id: fromUser._id }, { available_creditline: lessCredit }).exec();
        //transaction_id generation
        let lastTrans: any = await db.transactions.find().sort({ createdAt: -1 }).limit(1);
        if (!lastTrans.length) {
            trn_id = trn_id + 1000;
        } else {
            let count = lastTrans[0].transaction_id.replace("TR", "");
            let addCount = parseInt(count) + 1;
            trn_id = "TR" + addCount;
        }

        await db.transactions.create({
            from_user: fromUser._id,
            upi_id: toUser,
            transaction_amount: parseFloat(args.amount).toFixed(2),
            before_balance: fromUser.available_creditline,
            after_balance: lessCredit,
            transaction_id: trn_id,
            is_listed: true,
        });

        return { message: "transactions success" };
    } catch (err) {
        throw new GraphQLError(err.message, null, null, null);
    }
};
export default send_money_to_upi;
