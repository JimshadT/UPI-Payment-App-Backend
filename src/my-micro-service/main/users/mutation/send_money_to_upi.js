"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const gql_now_1 = require("gql-now");
const Error_Handler_1 = require("../../../helpers/Error_Handler");
const jwt_authentication_1 = require("../../../helpers/jwt_authentication");
const db_schemas_1 = require("../../../db-schemas");
var trn_id = "TR";
const db = new db_schemas_1.default();
const graphql_1 = require("graphql");
const send_money_to_upi = async (args, req, user) => {
    var _a;
    try {
        let fromUser = await db.users.findOne({ _id: user.id }).lean().exec();
        let toUser = args.upi_id;
        let payout_payload = {
            account_number: "3434866817852974",
            amount: args.amount * 100,
            currency: "INR",
            mode: "UPI",
            purpose: "payout",
            fund_account: { account_type: "vpa", vpa: { address: args.upi_id }, contact: { name: (_a = args.name) !== null && _a !== void 0 ? _a : "user" } },
        };
        let obj = new gql_now_1.GQLNow("https://api-ma-uat.kred.cloud/v1/razorpay-payouts", {
            "x-access-token": (0, jwt_authentication_1.default)({ id: fromUser === null || fromUser === void 0 ? void 0 : fromUser._id }),
        });
        let response = await obj.mutation("payout_to_vpa", payout_payload, "id entity fund_account_id status purpose");
        (0, Error_Handler_1.default)(typeof response.errors !== "undefined", response.errors);
        (0, Error_Handler_1.default)(args.amount > fromUser.available_creditline, "Insufficient money");
        let lessCredit = parseFloat((fromUser.available_creditline - args.amount).toFixed(2));
        //let addCredit:number = parseFloat((toUser.available_creditline + args.amount).toFixed(2));
        await db.users.updateOne({ _id: fromUser._id }, { available_creditline: lessCredit }).exec();
        //transaction_id generation
        let lastTrans = await db.transactions.find().sort({ createdAt: -1 }).limit(1);
        if (!lastTrans.length) {
            trn_id = trn_id + 1000;
        }
        else {
            let count = lastTrans[0].transaction_id.replace("TR", "");
            let addCount = parseInt(count) + 1;
            trn_id = "TR" + addCount;
        }
        await db.transactions.create({
            from_user: fromUser._id,
            upi_id: toUser,
            transaction_amount: parseFloat(args.amount).toFixed(2),
            before_balance: fromUser.available_creditline,
            after_balance: lessCredit,
            transaction_id: trn_id,
            is_listed: true,
        });
        return { message: "transactions success" };
    }
    catch (err) {
        throw new graphql_1.GraphQLError(err.message, null, null, null);
    }
};
exports.default = send_money_to_upi;
