import myAssert from "../../../helpers/Error_Handler";
import DBInterface from "../../../db-schemas";
const db = new DBInterface();
const { GraphQLError } = require("graphql");

const view_user_profile = async (args: any, req: any, user: any) => {
    try {
        let usr: any = await db.users.findOne({ _id: user.id });
        myAssert(usr === null, "user does not found");

        return usr;
    } catch (err) {
        throw new GraphQLError(err.message, null, null, null);
    }
};

export default view_user_profile;
