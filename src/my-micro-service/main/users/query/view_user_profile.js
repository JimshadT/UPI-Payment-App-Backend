"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Error_Handler_1 = require("../../../helpers/Error_Handler");
const db_schemas_1 = require("../../../db-schemas");
const db = new db_schemas_1.default();
const { GraphQLError } = require("graphql");
const view_user_profile = async (args, req, user) => {
    try {
        let usr = await db.users.find({ _id: user.id });
        (0, Error_Handler_1.default)(usr === null, "user does not found");
        let username = usr[0].username;
        let email = usr[0].email;
        return { username: username, email: email };
    }
    catch (err) {
        throw new GraphQLError(err.message, null, null, null);
    }
};
exports.default = view_user_profile;
