"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const MAPPGQLRoot_1 = require("./MAPPGQLRoot");
const MAPPCors_1 = require("./MAPPCors");
const express = require("express");
const service_validation_1 = require("./rest-apis/service-validation");
var MAPP = express.Router();
MAPP.options("*", MAPPCors_1.default);
MAPP.use(MAPPCors_1.default);
MAPP.get("/service", service_validation_1.service_validation);
MAPP.use(MAPPGQLRoot_1.default);
exports.default = MAPP;
