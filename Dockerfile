FROM alpine:latest as build

RUN mkdir -p /usr/src/node/app/ && \
    apk add --no-cache --update npm git

WORKDIR /usr/src/node/app

COPY . .
RUN rm -fR node_modules && rm -fR .git

RUN npm ci && \
    npm i -g typescript@4.0.5 && tsc -p . && npm r -g typescript && \
    rm -rf ./*.ts ./**/*.ts ./**/**/*.ts ./**/**/**/*.ts ./**/**/**/**/*.ts  ./**/**/**/**/**/*.ts

FROM alpine:latest

ENV TZ=Asia/Kolkata

RUN mkdir -p /home/node/app/ && \
    apk add --no-cache --update nodejs tzdata && \ 
    addgroup -S node && adduser -S node -G node && \
    ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

WORKDIR /home/node/app

COPY --from=build /usr/src/node/app ./

USER node
